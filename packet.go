package acc_client

import (
	"bytes"
	"fmt"
	"math"
	"time"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func handle(err *error) {
	if r := recover(); r != nil {
		*err = r.(error)
	}
}

func RegisterCommandApplicationRequest(displayName, password, commandPassword string, updateInterval time.Duration) (b []byte, err error) {
	defer handle(&err)

	var buf bytes.Buffer
	check(writeByte(&buf, byte(RequestRegisterCommandApplication)))
	check(writeByte(&buf, ProtocolVersion))
	check(writeString(&buf, displayName))
	check(writeString(&buf, password))
	check(writeDurationMs(&buf, updateInterval))
	check(writeString(&buf, commandPassword))
	b = buf.Bytes()

	return
}

func MarshalUnregisterCommandApplication(connectionId int32) (b []byte, err error) {
	defer handle(&err)

	var buf bytes.Buffer
	check(writeByte(&buf, byte(RequestUnregisterCommandApplication)))
	check(write(&buf, connectionId))
	b = buf.Bytes()

	return
}

func UnmarshalRegistrationResult(buf *bytes.Buffer) (connectionId int32, connectionSuccess, connectionReadonly bool, err error) {
	defer handle(&err)

	var msg string
	var success, readonly byte
	check(read(buf, &connectionId))
	check(read(buf, &success))
	connectionSuccess = success > 0
	check(read(buf, &readonly))
	connectionReadonly = readonly == 0
	check(readString(buf, &msg))

	if msg != "" {
		err = fmt.Errorf(msg)
	}

	return
}

func MarshalEntryListRequest(connectionId int32) (b []byte, err error) {
	defer handle(&err)

	var buf bytes.Buffer
	check(writeByte(&buf, byte(RequestEntryList)))
	check(write(&buf, connectionId))
	b = buf.Bytes()

	return
}

func UnmarshalEntryListResponse(buf *bytes.Buffer) (connectionId int32, entryList EntryList, err error) {
	defer handle(&err)

	var entryCount uint16
	check(read(buf, &connectionId))
	check(read(buf, &entryCount))

	entryList = make(EntryList, entryCount)
	for i := range entryList {
		check(read(buf, &entryList[i]))
	}

	return
}

func UnmarshalEntryListCarResponse(buf *bytes.Buffer) (car EntryListCar, err error) {
	defer handle(&err)

	check(read(buf, &car.Id))
	check(read(buf, &car.Model)) // Byte sized car model
	check(readString(buf, &car.TeamName))
	check(read(buf, &car.RaceNumber))
	check(read(buf, &car.CupCategory)) // Cup: Overall/Pro = 0, ProAm = 1, Am = 2, Silver = 3, National = 4
	check(read(buf, &car.CurrentDriverId))
	check(read(buf, &car.Nationality))

	var driverCount uint8
	check(read(buf, &driverCount))
	car.Drivers = make([]Driver, driverCount)

	for i := range car.Drivers {
		check(readString(buf, &car.Drivers[i].FirstName))
		check(readString(buf, &car.Drivers[i].LastName))
		check(readString(buf, &car.Drivers[i].ShortName))
		check(read(buf, &car.Drivers[i].Category))    // Platinum = 3, Gold = 2, Silver = 1, Bronze = 0
		check(read(buf, &car.Drivers[i].Nationality)) // new in 1.13.11
	}

	return
}

func MarshalTrackDataRequest(connectionId int32) (b []byte, err error) {
	defer handle(&err)

	var buf bytes.Buffer
	check(writeByte(&buf, byte(RequestTrackData)))
	check(write(&buf, connectionId))
	b = buf.Bytes()

	return
}

func UnmarshalTrackDataResponse(buf *bytes.Buffer) (connectionId int32, trackData TrackData, err error) {
	defer handle(&err)

	check(read(buf, &connectionId))
	check(readString(buf, &trackData.Name))
	check(read(buf, &trackData.Id))
	check(read(buf, &trackData.Length))

	var cameraSetsCount uint8
	check(read(buf, &cameraSetsCount))
	trackData.CameraSets = make(map[string][]string)

	for i := uint8(0); i < cameraSetsCount; i++ {
		var cameraSetName string
		check(readString(buf, &cameraSetName))

		var cameraCount uint8
		check(read(buf, &cameraCount))

		cameraSet := make([]string, cameraCount)
		for s := range cameraSet {
			check(readString(buf, &cameraSet[s]))
		}

		trackData.CameraSets[cameraSetName] = cameraSet
	}

	var hudPagesCount uint8
	check(read(buf, &hudPagesCount))

	trackData.HUDPages = make([]string, hudPagesCount)
	for i := range trackData.HUDPages {
		check(readString(buf, &trackData.HUDPages[i]))
	}

	return
}

func UnmarshalRealtimeUpdate(buf *bytes.Buffer) (update RealtimeUpdate, err error) {
	defer handle(&err)

	check(read(buf, &update.EventIndex))
	check(read(buf, &update.SessionIndex))
	check(read(buf, &update.SessionType))
	check(read(buf, &update.Phase))
	check(read(buf, &update.SessionTime))
	check(read(buf, &update.SessionEndTime))
	check(read(buf, &update.FocusedCarIndex))
	check(readString(buf, &update.ActiveCameraSet))
	check(readString(buf, &update.ActiveCamera))
	check(readString(buf, &update.CurrentHUDPage))
	check(read(buf, &update.IsReplayPlaying))
	if update.IsReplayPlaying {
		check(read(buf, &update.ReplaySessionTime))
		check(read(buf, &update.ReplayRemainingTime))
	}
	check(read(buf, &update.TimeOfDay))
	check(read(buf, &update.AmbientTemp))
	check(read(buf, &update.TrackTemp))
	check(read(buf, &update.Clouds))
	update.Clouds /= 10.0
	check(read(buf, &update.RainLevel))
	update.RainLevel /= 10.0
	check(read(buf, &update.Wetness))
	update.Wetness /= 10.0

	update.BestSessionLap = Lap{}
	unmarshalLap(buf, &update.BestSessionLap)

	return
}

func UnmarshalRealtimeCarUpdate(buf *bytes.Buffer) (realtimeCarUpdate RealtimeCarUpdate, err error) {
	defer handle(&err)

	check(read(buf, &realtimeCarUpdate.Id))
	check(read(buf, &realtimeCarUpdate.DriverId)) // Driver swap will make this change
	check(read(buf, &realtimeCarUpdate.DriverCount))
	check(read(buf, &realtimeCarUpdate.Gear))
	realtimeCarUpdate.Gear -= 1 // Kunos: -2 makes the R -1, N 0 and the rest as-is
	// actually: it's -1
	check(read(buf, &realtimeCarUpdate.Yaw))
	check(read(buf, &realtimeCarUpdate.Roll))
	check(read(buf, &realtimeCarUpdate.Pitch))
	check(read(buf, &realtimeCarUpdate.CarLocation)) // - , Track, Pitlane, PitEntry, PitExit = 4
	check(read(buf, &realtimeCarUpdate.Speed))
	check(read(buf, &realtimeCarUpdate.Position))       // official P/Q/R position (1 based)
	check(read(buf, &realtimeCarUpdate.CupPosition))    // official P/Q/R position (1 based)
	check(read(buf, &realtimeCarUpdate.TrackPosition))  // position on track (1 based)
	check(read(buf, &realtimeCarUpdate.SplinePosition)) // track position between 0.0 and 1.0
	check(read(buf, &realtimeCarUpdate.Laps))
	check(read(buf, &realtimeCarUpdate.Delta)) // Realtime delta to best session lap
	realtimeCarUpdate.BestSessionLap = Lap{}
	unmarshalLap(buf, &realtimeCarUpdate.BestSessionLap)
	realtimeCarUpdate.LastLap = Lap{}
	unmarshalLap(buf, &realtimeCarUpdate.LastLap)
	realtimeCarUpdate.CurrentLap = Lap{}
	unmarshalLap(buf, &realtimeCarUpdate.CurrentLap)

	// TODO: Ask for entry list update if car or driver is unknown

	return
}

func UnmarshalBroadcastingEvent(buf *bytes.Buffer) (event BroadcastingEvent, err error) {
	defer handle(&err)

	check(read(buf, &event.Type))
	check(readString(buf, &event.Message))
	check(read(buf, &event.TimeMs))
	check(read(buf, &event.CarId))

	return
}

func MarshalReplayRequest(connectionId int32, startTime float32, duration time.Duration, focusedCar int32, cameraSet, camera string) (b []byte, err error) {
	defer handle(&err)

	var buf bytes.Buffer
	check(writeByte(&buf, byte(RequestInstantReplay)))
	check(write(&buf, connectionId))
	check(write(&buf, startTime))
	check(writeDurationFloat(&buf, duration))
	check(write(&buf, focusedCar))
	check(writeString(&buf, cameraSet))
	check(writeString(&buf, camera))
	b = buf.Bytes()

	return
}

func MarshalHUDPageRequest(connectionId int32, hudPage string) (b []byte, err error) {
	defer handle(&err)

	var buf bytes.Buffer
	check(writeByte(&buf, byte(RequestHUDPage)))
	check(write(&buf, connectionId))
	check(writeString(&buf, hudPage))
	b = buf.Bytes()

	return
}

func MarshalFocusChangeRequest(connectionId int32, carIndex *uint16, cameraSet, camera string) (b []byte, err error) {
	defer handle(&err)

	var buf bytes.Buffer
	check(writeByte(&buf, byte(RequestFocusChange)))
	check(write(&buf, connectionId))

	if carIndex == nil {
		check(writeByte(&buf, 0))
	} else {
		check(writeByte(&buf, 1))
		check(write(&buf, carIndex))
	}

	if cameraSet == "" || camera == "" {
		check(writeByte(&buf, 0))
	} else {
		check(writeByte(&buf, 1))
		check(writeString(&buf, cameraSet))
		check(writeString(&buf, camera))
	}
	b = buf.Bytes()

	return
}

func unmarshalLap(buf *bytes.Buffer, lap *Lap) {
	check(read(buf, &lap.LapTimeMs))
	if lap.LapTimeMs == math.MaxInt32 {
		lap.LapTimeMs = -1
	}

	check(read(buf, &lap.CarId))
	check(read(buf, &lap.DriverId))

	var splitCount uint8
	check(read(buf, &splitCount))
	lap.Splits = make([]int32, splitCount)
	for i := range lap.Splits {
		check(read(buf, &lap.Splits[i]))
		if lap.Splits[i] == math.MaxInt32 {
			lap.Splits[i] = -1
		}
	}

	check(read(buf, &lap.IsInvalid))
	check(read(buf, &lap.IsValidForBest))
	check(read(buf, &lap.IsOutLap))
	check(read(buf, &lap.IsInLap))

	if lap.IsOutLap {
		lap.Type = LapTypeOutLap
	} else if lap.IsInLap {
		lap.Type = LapTypeInLap
	} else {
		lap.Type = LapTypeRegular
	}
}
