package acc_client

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/rs/zerolog"
	"net"
	"time"
)

const ReadBufferSize = 32 * 1024

var ErrNotConnected = errors.New("not connected")

var (
	entryListCars        = make(map[uint16]EntryListCar)
	lastEntrylistRequest time.Time
)

type Client struct {
	conn         *net.UDPConn
	connectionId int32

	timeout       time.Duration
	stopListening bool

	Logger               zerolog.Logger
	OnConnected          func(connectionId int32, success, readOnly bool)
	OnDisconnected       func()
	OnRealtimeUpdate     func(realtimeUpdate RealtimeUpdate)
	OnRealtimeCarUpdate  func(realtimeCarUpdate RealtimeCarUpdate)
	OnEntryListUpdate    func(entryList EntryList)
	OnEntryListCarUpdate func(entryListCar EntryListCar)
	OnTrackUpdate        func(trackData TrackData)
	OnBroadcastingEvent  func(event BroadcastingEvent)
}

func (client *Client) ConnectAndListen(address, name, password, commandPassword string, realtimeUpdateInterval, timeout time.Duration) {
	client.timeout = timeout

	err := client.connect(address, name, password, commandPassword, realtimeUpdateInterval)
	if err == nil {
		client.Logger.Debug().Msg("udp connected, starting listener")
		err = client.listen()
		client.Logger.Debug().Msg("listener exited")
	}
	if err != nil {
		client.Logger.Warn().Err(err).Msg("listener exited with error")
	}
	client.disconnect()
}

func (client *Client) connect(address, name, password, commandPassword string, realtimeUpdateInterval time.Duration) error {
	client.stopListening = false

	addr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		return err
	}

	client.conn, err = net.DialUDP("udp", nil, addr)
	if err != nil {
		return err
	}

	req, err := RegisterCommandApplicationRequest(name, password, commandPassword, realtimeUpdateInterval)
	if err != nil {
		return err
	}

	err = client.conn.SetDeadline(time.Now().Add(client.timeout))
	if err != nil {
		return err
	}

	return client.send(req)
}

func (client *Client) listen() error {
	var buf [ReadBufferSize]byte

	for !client.stopListening {
		err := client.conn.SetDeadline(time.Now().Add(client.timeout))
		if err != nil {
			return err
		}

		n, err := client.conn.Read(buf[:])
		if err != nil {
			client.stopListening = true
			return fmt.Errorf("error while reading data: %w", err)
		}
		if n == ReadBufferSize {
			return fmt.Errorf("exceeded read buffer size")
		}

		readBuffer := bytes.NewBuffer(buf[:n])
		msgType, err := readBuffer.ReadByte()
		if err != nil {
			client.stopListening = true
			return fmt.Errorf("error parsing read buffer: %w", err)
		}

		switch ResponseType(msgType) {
		case ResponseRegistrationResult:
			connectionId, success, readOnly, err := UnmarshalRegistrationResult(readBuffer)
			if err != nil {
				client.stopListening = true
				return fmt.Errorf("registration result: %w", err)
			}
			client.connectionId = connectionId
			if client.OnConnected != nil {
				client.OnConnected(connectionId, success, readOnly)
			}

			err = client.RequestEntryList()
			if err != nil {
				return fmt.Errorf("request entry list failed: %w", err)
			}
			err = client.RequestTrackData()
			if err != nil {
				return fmt.Errorf("request track data failed: %w", err)
			}

		case ResponseEntryList:
			_, update, err := UnmarshalEntryListResponse(readBuffer)
			if err != nil {
				return fmt.Errorf("entry list response: %w", err)
			}

			entryListCars = make(map[uint16]EntryListCar, len(update))
			for _, id := range update {
				entryListCars[id] = EntryListCar{Id: id}
			}

			if client.OnEntryListUpdate != nil {
				client.OnEntryListUpdate(update)
			}

		case ResponseEntryListCar:
			update, err := UnmarshalEntryListCarResponse(readBuffer)
			if err != nil {
				return fmt.Errorf("entry list entry response: %w", err)
			}

			if _, ok := entryListCars[update.Id]; !ok {
				client.Logger.Warn().Msgf("Entry list update for unknown carIndex %d", update.Id)
			} else {
				entryListCars[update.Id] = update
			}

			if client.OnEntryListCarUpdate != nil {
				client.OnEntryListCarUpdate(update)
			}

		case ResponseRealtimeUpdate:
			if client.OnRealtimeUpdate != nil {
				update, err := UnmarshalRealtimeUpdate(readBuffer)
				if err != nil {
					return fmt.Errorf("update response: %w", err)
				}
				client.OnRealtimeUpdate(update)
			}

		case ResponseRealtimeCarUpdate:
			update, err := UnmarshalRealtimeCarUpdate(readBuffer)
			if err != nil {
				return fmt.Errorf("car update response: %w", err)
			}

			if carEntry, ok := entryListCars[update.Id]; !ok || len(carEntry.Drivers) != int(update.DriverCount) {
				if time.Since(lastEntrylistRequest) > 1*time.Second {
					lastEntrylistRequest = time.Now()
					client.Logger.Info().Msgf("CarUpdate %d|%d not known, will ask for new EntryList", update.Id, update.DriverId)
					err = client.RequestEntryList()
					if err != nil {
						client.Logger.Warn().Err(err).Msg("entry list request on incomplete RealtimeCarUpdate failed")
					}
				}
			} else if client.OnRealtimeCarUpdate != nil {
				// only handle update if entryListCar is complete
				client.OnRealtimeCarUpdate(update)
			}

		case ResponseTrackData:
			if client.OnTrackUpdate != nil {
				_, update, err := UnmarshalTrackDataResponse(readBuffer)
				if err != nil {
					return fmt.Errorf("track data response: %w", err)
				}
				client.OnTrackUpdate(update)
			}

		case ResponseBroadcastingEvent:
			if client.OnBroadcastingEvent != nil {
				update, err := UnmarshalBroadcastingEvent(readBuffer)
				if err != nil {
					return fmt.Errorf("broadcast response: %w", err)
				}
				client.OnBroadcastingEvent(update)
			}

		default:
			return fmt.Errorf("unsupported message type: %x", msgType)
		}
	}

	return nil
}

func (client *Client) RequestDisconnect() {
	client.stopListening = true
	// actual disconnect will be handled by the main loop exiting
}

func (client *Client) disconnect() {
	req, err := MarshalUnregisterCommandApplication(client.connectionId)
	if err != nil {
		client.Logger.Warn().Err(err).Msg("error marshalling disconnect request")
		return
	}

	err = client.send(req)
	if err != nil {
		client.Logger.Warn().Err(err).Msg("disconnect request")
	}

	err = client.conn.Close()
	if err != nil {
		client.Logger.Warn().Err(err).Msg("error closing client connection")
	}
	client.conn = nil

	if client.OnDisconnected != nil {
		client.OnDisconnected()
	}
}

func (client *Client) send(req []byte) error {
	n, err := client.conn.Write(req)
	if err != nil {
		return fmt.Errorf("error sending request: %w", err)
	} else if n < len(req) {
		return fmt.Errorf("could only partially write request")
	}
	return nil
}

func (client *Client) RequestTrackData() error {
	if client.stopListening {
		return ErrNotConnected
	}

	req, err := MarshalTrackDataRequest(client.connectionId)
	if err != nil {
		return fmt.Errorf("error marshalling track data request: %w", err)
	}

	return client.send(req)
}

func (client *Client) RequestEntryList() error {
	if client.stopListening {
		return ErrNotConnected
	}

	req, err := MarshalEntryListRequest(client.connectionId)
	if err != nil {
		return fmt.Errorf("error marshalling entry list request: %w", err)
	}

	return client.send(req)
}

func (client *Client) RequestInstantReplay(time float32, duration time.Duration, car int32, cameraSet, camera string) error {
	if client.stopListening {
		return ErrNotConnected
	}

	req, err := MarshalReplayRequest(client.connectionId, time, duration, car, cameraSet, camera)
	if err != nil {
		return fmt.Errorf("error marshalling replay request: %w", err)
	}

	return client.send(req)
}

func (client *Client) RequestHUDPage(hudPage string) error {
	if client.stopListening {
		return ErrNotConnected
	}

	req, err := MarshalHUDPageRequest(client.connectionId, hudPage)
	if err != nil {
		return fmt.Errorf("error marshalling hud page request: %w", err)
	}

	return client.send(req)
}

func (client *Client) RequestFocusChange(carIndex *uint16, cameraSet, camera string) error {
	if client.stopListening {
		return ErrNotConnected
	}

	req, err := MarshalFocusChangeRequest(client.connectionId, carIndex, cameraSet, camera)
	if err != nil {
		return fmt.Errorf("error marshalling focus change request: %w", err)
	}

	return client.send(req)
}
