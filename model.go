package acc_client

type EntryList []uint16

type EntryListCar struct {
	Id              uint16
	Model           byte
	TeamName        string
	RaceNumber      int32
	CupCategory     CupCategory
	CurrentDriverId int8
	Nationality     Nationality
	Drivers         []Driver
}

type Driver struct {
	FirstName   string
	LastName    string
	ShortName   string
	Category    DriverCategory
	Nationality Nationality
}

type TrackData struct {
	Name       string
	Id         int32
	Length     int32
	CameraSets map[string][]string
	HUDPages   []string
}

type RealtimeUpdate struct {
	EventIndex          uint16
	SessionIndex        uint16
	SessionType         SessionType
	Phase               SessionPhase
	SessionTime         float32
	SessionEndTime      float32
	FocusedCarIndex     int32
	ActiveCameraSet     string
	ActiveCamera        string
	CurrentHUDPage      string
	IsReplayPlaying     bool
	TimeOfDay           int32
	AmbientTemp         int8
	TrackTemp           int8
	Clouds              byte
	RainLevel           byte
	Wetness             byte
	BestSessionLap      Lap
	ReplaySessionTime   float32
	ReplayRemainingTime float32
}

type RealtimeCarUpdate struct {
	Id             uint16
	DriverId       uint16
	DriverCount    uint8
	Gear           int8
	Yaw            float32
	Roll           float32
	Pitch          float32
	CarLocation    CarLocation
	Speed          uint16
	Position       uint16
	CupPosition    uint16
	TrackPosition  uint16
	SplinePosition float32
	Laps           uint16
	Delta          int32
	BestSessionLap Lap
	LastLap        Lap
	CurrentLap     Lap
}

type Lap struct {
	LapTimeMs      int32
	CarId          uint16
	DriverId       uint16
	Splits         []int32
	IsInvalid      bool
	IsValidForBest bool
	IsOutLap       bool
	IsInLap        bool
	Type           LapType
}

type BroadcastingEvent struct {
	Type    BroadcastingCarEventType
	Message string
	TimeMs  int32
	CarId   int32
}
