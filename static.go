package acc_client

import "fmt"

const (
	ProtocolVersion byte   = 4
	SDKVersion      string = "1.7.12"
)

type RequestType byte

const (
	RequestRegisterCommandApplication   RequestType = 1
	RequestUnregisterCommandApplication RequestType = 9
	RequestEntryList                    RequestType = 10
	RequestTrackData                    RequestType = 11
	RequestHUDPage                      RequestType = 49
	RequestFocusChange                  RequestType = 50
	RequestInstantReplay                RequestType = 51
	RequestPlayManualReplayHighlight    RequestType = 52 // TODO, but planned
	RequestSaveManualReplayHighlight    RequestType = 60 // TODO, but planned: saving manual replays gives distributed clients the possibility to see the play the same replay
)

type ResponseType byte

const (
	ResponseRegistrationResult ResponseType = 1
	ResponseRealtimeUpdate     ResponseType = 2
	ResponseRealtimeCarUpdate  ResponseType = 3
	ResponseEntryList          ResponseType = 4
	ResponseTrackData          ResponseType = 5
	ResponseEntryListCar       ResponseType = 6
	ResponseBroadcastingEvent  ResponseType = 7
)

var responseTypeString = map[ResponseType]string{
	ResponseRegistrationResult: "ResponseRegistrationResult",
	ResponseRealtimeUpdate:     "ResponseRealtimeUpdate",
	ResponseRealtimeCarUpdate:  "ResponseRealtimeCarUpdate",
	ResponseEntryList:          "ResponseEntryList",
	ResponseTrackData:          "ResponseTrackData",
	ResponseEntryListCar:       "ResponseEntryListCar",
	ResponseBroadcastingEvent:  "ResponseBroadcastingEvent",
}

func (rt ResponseType) String() string {
	if s, ok := responseTypeString[rt]; ok {
		return s
	}
	return fmt.Sprintf("ResponseUnknown(%d)", rt)
}

type DriverCategory byte

const (
	DriverCategoryBronze   DriverCategory = 0
	DriverCategorySilver   DriverCategory = 1
	DriverCategoryGold     DriverCategory = 2
	DriverCategoryPlatinum DriverCategory = 3
	DriverCategoryError    DriverCategory = 255
)

type CupCategory byte

const (
	CupCategoryPro      CupCategory = 0
	CupCategoryProAm    CupCategory = 1
	CupCategoryAm       CupCategory = 2
	CupCategorySilver   CupCategory = 3
	CupCategoryNational CupCategory = 4
)

type LapType byte

const (
	LapTypeError   LapType = 0
	LapTypeOutLap  LapType = 1
	LapTypeRegular LapType = 2
	LapTypeInLap   LapType = 3
)

type CarLocation uint8

const (
	CarLocationNone     CarLocation = 0
	CarLocationTrack    CarLocation = 1
	CarLocationPitlane  CarLocation = 2
	CarLocationPitEntry CarLocation = 3
	CarLocationPitExit  CarLocation = 4
)

type SessionPhase byte

const (
	SessionPhaseNone         SessionPhase = 0
	SessionPhaseStarting     SessionPhase = 1
	SessionPhasePreFormation SessionPhase = 2
	SessionPhaseFormationLap SessionPhase = 3
	SessionPhasePreSession   SessionPhase = 4
	SessionPhaseSession      SessionPhase = 5
	SessionPhaseSessionOver  SessionPhase = 6
	SessionPhasePostSession  SessionPhase = 7
	SessionPhaseResultUI     SessionPhase = 8
)

type SessionType byte

const (
	SessionTypePractice        SessionType = 0
	SessionTypeQualifying      SessionType = 4
	SessionTypeSuperpole       SessionType = 9
	SessionTypeRace            SessionType = 10
	SessionTypeHotlap          SessionType = 11
	SessionTypeHotstint        SessionType = 12
	SessionTypeHotlapSuperpole SessionType = 13
	SessionTypeReplay          SessionType = 14
)

type BroadcastingCarEventType byte

const (
	BroadcastingCarEventTypeNone            BroadcastingCarEventType = 0
	BroadcastingCarEventTypeGreenFlag       BroadcastingCarEventType = 1
	BroadcastingCarEventTypeSessionOver     BroadcastingCarEventType = 2
	BroadcastingCarEventTypePenaltyCommMsg  BroadcastingCarEventType = 3
	BroadcastingCarEventTypeAccident        BroadcastingCarEventType = 4
	BroadcastingCarEventTypeLapCompleted    BroadcastingCarEventType = 5
	BroadcastingCarEventTypeBestSessionLap  BroadcastingCarEventType = 6
	BroadcastingCarEventTypeBestPersonalLap BroadcastingCarEventType = 7
)

type Nationality uint16

const (
	NationalityAny             Nationality = 0
	NationalityItaly           Nationality = 1
	NationalityGermany         Nationality = 2
	NationalityFrance          Nationality = 3
	NationalitySpain           Nationality = 4
	NationalityGreatBritain    Nationality = 5
	NationalityHungary         Nationality = 6
	NationalityBelgium         Nationality = 7
	NationalitySwitzerland     Nationality = 8
	NationalityAustria         Nationality = 9
	NationalityRussia          Nationality = 10
	NationalityThailand        Nationality = 11
	NationalityNetherlands     Nationality = 12
	NationalityPoland          Nationality = 13
	NationalityArgentina       Nationality = 14
	NationalityMonaco          Nationality = 15
	NationalityIreland         Nationality = 16
	NationalityBrazil          Nationality = 17
	NationalitySouthAfrica     Nationality = 18
	NationalityPuertoRico      Nationality = 19
	NationalitySlovakia        Nationality = 20
	NationalityOman            Nationality = 21
	NationalityGreece          Nationality = 22
	NationalitySaudiArabia     Nationality = 23
	NationalityNorway          Nationality = 24
	NationalityTurkey          Nationality = 25
	NationalitySouthKorea      Nationality = 26
	NationalityLebanon         Nationality = 27
	NationalityArmenia         Nationality = 28
	NationalityMexico          Nationality = 29
	NationalitySweden          Nationality = 30
	NationalityFinland         Nationality = 31
	NationalityDenmark         Nationality = 32
	NationalityCroatia         Nationality = 33
	NationalityCanada          Nationality = 34
	NationalityChina           Nationality = 35
	NationalityPortugal        Nationality = 36
	NationalitySingapore       Nationality = 37
	NationalityIndonesia       Nationality = 38
	NationalityUSA             Nationality = 39
	NationalityNewZealand      Nationality = 40
	NationalityAustralia       Nationality = 41
	NationalitySanMarino       Nationality = 42
	NationalityUAE             Nationality = 43
	NationalityLuxembourg      Nationality = 44
	NationalityKuwait          Nationality = 45
	NationalityHongKong        Nationality = 46
	NationalityColombia        Nationality = 47
	NationalityJapan           Nationality = 48
	NationalityAndorra         Nationality = 49
	NationalityAzerbaijan      Nationality = 50
	NationalityBulgaria        Nationality = 51
	NationalityCuba            Nationality = 52
	NationalityCzechRepublic   Nationality = 53
	NationalityEstonia         Nationality = 54
	NationalityGeorgia         Nationality = 55
	NationalityIndia           Nationality = 56
	NationalityIsrael          Nationality = 57
	NationalityJamaica         Nationality = 58
	NationalityLatvia          Nationality = 59
	NationalityLithuania       Nationality = 60
	NationalityMacau           Nationality = 61
	NationalityMalaysia        Nationality = 62
	NationalityNepal           Nationality = 63
	NationalityNewCaledonia    Nationality = 64
	NationalityNigeria         Nationality = 65
	NationalityNorthernIreland Nationality = 66
	NationalityPapuaNewGuinea  Nationality = 67
	NationalityPhilippines     Nationality = 68
	NationalityQatar           Nationality = 69
	NationalityRomania         Nationality = 70
	NationalityScotland        Nationality = 71
	NationalitySerbia          Nationality = 72
	NationalitySlovenia        Nationality = 73
	NationalityTaiwan          Nationality = 74
	NationalityUkraine         Nationality = 75
	NationalityVenezuela       Nationality = 76
	NationalityWales           Nationality = 77
	NationalityIran            Nationality = 78
	NationalityBahrain         Nationality = 79
	NationalityZimbabwe        Nationality = 80
	NationalityChineseTaipei   Nationality = 81
	NationalityChile           Nationality = 82
	NationalityUruguay         Nationality = 83
	NationalityMadagascar      Nationality = 84
)
