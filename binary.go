package acc_client

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"time"
)

func write(buf *bytes.Buffer, i interface{}) error {
	err := binary.Write(buf, binary.LittleEndian, i)
	if err != nil {
		return fmt.Errorf("write interface to buffer: %w", err)
	}
	return nil
}

func read(buf *bytes.Buffer, i interface{}) error {
	err := binary.Read(buf, binary.LittleEndian, i)
	if err != nil {
		return fmt.Errorf("read interface from buffer: %w", err)
	}
	return nil
}

func writeByte(buf *bytes.Buffer, b byte) error {
	err := buf.WriteByte(b)
	if err != nil {
		return fmt.Errorf("write byte to buffer: %w", err)
	}
	return nil
}

func writeString(buf *bytes.Buffer, s string) error {
	err := binary.Write(buf, binary.LittleEndian, uint16(len(s)))
	if err != nil {
		return fmt.Errorf("write string length to buffer: %w", err)
	}
	_, err = buf.Write([]byte(s))
	if err != nil {
		return fmt.Errorf("write string to buffer: %w", err)
	}
	return nil
}

func readString(buf *bytes.Buffer, s *string) error {
	var l uint16
	err := binary.Read(buf, binary.LittleEndian, &l)
	if err != nil {
		return fmt.Errorf("read string length from buffer: %w", err)
	}

	sb := make([]byte, l)
	err = binary.Read(buf, binary.LittleEndian, &sb)
	if err != nil {
		return fmt.Errorf("read string from buffer: %w", err)
	}
	*s = string(sb)
	return nil
}

func writeDurationFloat(buf *bytes.Buffer, d time.Duration) error {
	err := binary.Write(buf, binary.LittleEndian, float32(d.Milliseconds()))
	if err != nil {
		return fmt.Errorf("write duration to buffer as ms: %w", err)
	}
	return nil
}

func writeDurationMs(buf *bytes.Buffer, d time.Duration) error {
	err := binary.Write(buf, binary.LittleEndian, int32(d.Milliseconds()))
	if err != nil {
		return fmt.Errorf("write duration to buffer as ms: %w", err)
	}
	return nil
}
